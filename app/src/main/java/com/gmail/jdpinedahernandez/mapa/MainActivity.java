package com.gmail.jdpinedahernandez.mapa;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;

import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.gmail.jdpinedahernandez.mapa.Models.DTO.UserDTO;
import com.gmail.jdpinedahernandez.mapa.Utils.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    ArrayList<UserDTO> userarray= new ArrayList<>();
    private GestureDetector gestureDetector;
    private WindowManager windowManager;
    private ImageView img_boton_flotante;
    WindowManager.LayoutParams params;
    ProgressDialog progressDialog;
    String Tag = "MainActivity";
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_fragment);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        progressDialog = new ProgressDialog(MainActivity.this);
        context = MainActivity.this;
        progressDialog.setMessage(context.getResources().getString(R.string.download));
        /*Botón flotante usado para actualizar los marcadores en el mapa*/
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        gestureDetector = new GestureDetector(this, new SingleTapConfirm());
        img_boton_flotante = new ImageView(this);
        img_boton_flotante.setImageResource(R.mipmap.ic_launcher);
        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_DRAWN_APPLICATION,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 100;
        windowManager.addView(img_boton_flotante, params);
        img_boton_flotante.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.i(Tag,"Touch");
                            if(userarray.size()>0)
                                RandomLatLong();
                            else {
                                MarkersRequest();
                            }
                        }
                    });
                    return true;
                } else {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            initialX = params.x;
                            initialY = params.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            return true;
                        case MotionEvent.ACTION_UP:
                            return true;
                        case MotionEvent.ACTION_MOVE:
                            params.x = initialX + (int) (event.getRawX() - initialTouchX);
                            params.y = initialY + (int) (event.getRawY() - initialTouchY);
                            windowManager.updateViewLayout(img_boton_flotante, params);

                            return true;
                    }
                }
                return false;
            }
        });
        /*Botón flotante usado para actualizar los marcadores en el mapa*/
    }
    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        /*Traer Json con los datos de los marcadores*/

        MarkersRequest();
    }
    public void MarkersRequest(){
        progressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        Log.d(Tag, "Request: "+Constants.url);
        JsonArrayRequest req = new JsonArrayRequest(Constants.url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        try{
                            for (int i=0;i<response.length();i++){
                                UserDTO userDTO = new UserDTO();
                                userDTO.setId(response.getJSONObject(i).getInt("id"));
                                userDTO.setName(response.getJSONObject(i).getString("name"));
                                JSONObject addressObject=response.getJSONObject(i).getJSONObject("address");
                                userDTO.setStreet(addressObject.getString("street"));
                                JSONObject geoObject=addressObject.getJSONObject("geo");
                                userDTO.setLat(geoObject.getString("lat"));
                                userDTO.setLng(geoObject.getString("lng"));
                                userarray.add(userDTO);
                            }
                            RandomLatLong();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(Tag,"Response: "+ response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(),
                                    context.getResources().getString(R.string.timeoutError), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(),
                                    context.getResources().getString(R.string.authFailureError), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getApplicationContext(),
                                    context.getResources().getString(R.string.serverError), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(),
                                    context.getResources().getString(R.string.networkError), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getApplicationContext(),
                                    context.getResources().getString(R.string.parseError), Toast.LENGTH_SHORT).show();
                        }
                         VolleyLog.d(Tag, "Error: " + error.getMessage());


                    }
                }
        );
        queue.getCache().remove(Constants.url);
        queue.add(req);
    }
    /*Elegir un punto al azar a donde se enfoca la cámara*/
    public void RandomLatLong(){
        Random r = new Random();
        int Low = 0;
        int High = userarray.size();
        int Result = r.nextInt(High - Low) + Low;
        LatLng latLng_marker = new LatLng(Double.parseDouble(userarray.get(Result).getLat())
                , Double.parseDouble(userarray.get(Result).getLng()));
        mMap.clear();

        CalculateMaxMinDistance(latLng_marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng_marker));
    }

    /*Calcula la máxima y mínima distancia entre latLng_marker y los demás marcadores*/
    public void CalculateMaxMinDistance(LatLng latLng_marker){
        ArrayList<Double> distancias =new ArrayList<>();
        int actual_position=0;
        for (int i=0;i<userarray.size();i++) {
            LatLng latLng_to_marker = new LatLng(Double.parseDouble(userarray.get(i).getLat())
                    , Double.parseDouble(userarray.get(i).getLng()));
            userarray.get(i).setDistance(SphericalUtil.computeDistanceBetween(latLng_marker, latLng_to_marker));

            if (userarray.get(i).getDistance() != 0.0)
                distancias.add(userarray.get(i).getDistance());
            else
                actual_position = i;

            Log.i(Tag,"distancia: " + String.valueOf(userarray.get(i).getDistance())
                    + " , " + userarray.get(i).getName()
                    + " , " + userarray.get(i).getLat()
                    + " , " + userarray.get(i).getLng());
            }
        int minIndex = distancias.indexOf(Collections.min(distancias));
        if (minIndex>=actual_position){
            minIndex++;
        }
        int maxIndex = distancias.indexOf(Collections.max(distancias));
        if (maxIndex>=actual_position){
            maxIndex++;
        }
        Log.i(Tag,"MinIndex: "+ String.valueOf(minIndex) + " , " + userarray.get(minIndex).getName());
        Log.i(Tag,"MaxIndex: "+ String.valueOf(maxIndex) + " , " + userarray.get(maxIndex).getName());

        PaintMarkers(minIndex,maxIndex,actual_position);
    }
    /*Muestra en el mapa los marcadores:
    * Amarillo es la referencia ubicado en la posición actual
    * Verde es marcador mas cercano
    * Azul es el marcador más lejano
    * Rojos son los demás marcadores*/
    public void PaintMarkers(int minIndex, int maxIndex, int actual_position){
        Marker marker;
        DecimalFormat df = new DecimalFormat("####0.00");
        for (int i=0;i<userarray.size();i++){
            LatLng latLng_marker1 = new LatLng(Double.parseDouble(userarray.get(i).getLat())
                    , Double.parseDouble(userarray.get(i).getLng()));

            if (i==minIndex)
                marker = mMap.addMarker(new MarkerOptions().position(latLng_marker1)
                        .title(userarray.get(minIndex).getName()+" - "+context.getResources().getString(R.string.nearPoint))
                        .snippet(df.format(userarray.get(minIndex).getDistance())+context.getResources().getString(R.string.distancemesure))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            else if (i==maxIndex)
                marker = mMap.addMarker(new MarkerOptions().position(latLng_marker1)
                        .title(userarray.get(maxIndex).getName()+" - "+context.getResources().getString(R.string.farPoint))
                        .snippet(df.format(userarray.get(maxIndex).getDistance())+context.getResources().getString(R.string.distancemesure))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
            else if (i==actual_position) {
                marker = mMap.addMarker(new MarkerOptions().position(latLng_marker1)
                        .title(userarray.get(i).getName()+" - "+context.getResources().getString(R.string.reference))
                        .snippet(df.format(userarray.get(i).getDistance())+context.getResources().getString(R.string.distancemesure))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                marker.showInfoWindow();
            }
            else
                marker = mMap.addMarker(new MarkerOptions().position(latLng_marker1)
                        .title(userarray.get(i).getName())
                        .snippet(df.format(userarray.get(i).getDistance())+context.getResources().getString(R.string.distancemesure))
                        .alpha((float) 0.4));
            AnimateMarker(marker, latLng_marker1);

        }
    }
    public void AnimateMarker(final Marker marker, final LatLng pos) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(pos);
        startPoint.offset(0, -100);
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1500;
        final Interpolator interpolator = new BounceInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * pos.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * pos.latitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (img_boton_flotante != null) windowManager.removeView(img_boton_flotante);
    }
}
